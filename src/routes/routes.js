import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom"
import AdminPage from "../pages/admin"
import FrontPage from "../pages/front"
import AdminLayout from "../pages/layout/admin-layout.page"
import FrontLayout from "../pages/layout/front-layout.page"


const Routing = () => {
    const PrivateRoute = ({ component }) => {
        //  let component = props.component
        let is_logged_in = localStorage.getItem('access_token') ?? null;
        return (
            is_logged_in ? component : <Navigate to="/login" />
        )
    }
    return (
        <>
            <BrowserRouter>
                <Routes>
                    <Route path="/" element={<FrontLayout />}>
                        <Route index element={<FrontPage.HomePage />} />
                        <Route path='register' element={<FrontPage.RegisterPage />} />
                        <Route path='login' element={<FrontPage.LoginPage />} />

                        <Route path='sports' element={<FrontPage.SportPage />} />
                        <Route path='healths' element={<FrontPage.HealthPage />} />
                        <Route path='lifestyles' element={<FrontPage.LifeStyle />} />
                        <Route path='travels' element={<FrontPage.Travel />} />


                        <Route path='posts/detail/:slug' element={<FrontPage.DetailPage />} />
                       
                        <Route path="*" element={<FrontPage.ErrorPage />} />

                    </Route>
                    <Route path="/admin" element={<PrivateRoute component={<AdminLayout />} />}>
                        <Route index element={<AdminPage.DashboardPage />} />

                        <Route path="categories" element={<AdminPage.Category.CategoryLayout />}>
                            <Route index element={<AdminPage.Category.CategoryListPage />} />
                            <Route path="create" element={<AdminPage.Category.CategoryCreatePage />} >  </Route>
                            <Route path=":id" element={<AdminPage.Category.CategoriesEditPage />} >  </Route>
                        </Route>

                        <Route path="posts" element={<AdminPage.Post.PostsLayout />}>
                            <Route index element={<AdminPage.Post.PostsListPage />} />
                            <Route path="create" element={<AdminPage.Post.PostsCreatePage />} >  </Route>
                            <Route path=":id" element={<AdminPage.Post.PostEditPage />} >  </Route>
                        </Route>

                        <Route path="user" element={<AdminPage.User.UserLayout />}>
                            <Route index element={<AdminPage.User.UserList />} />
                        </Route>

                    </Route>
                </Routes>
            </BrowserRouter>
        </>
    )

}

export default Routing