import React from 'react'
import '../../assets/css/admin/admin.css'
import '@fortawesome/fontawesome-free/css/all.min.css'
import 'bootstrap'
import { Outlet } from 'react-router-dom'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';
import AdminPartials from '../admin/partials'
import { useEffect} from 'react'
import { httpGetRequest } from '../service/axios.service'

const AdminLayout = () => {
   // let [user, setUser] = useState()
  const getUserDetail = async(id) => {
   try{
    let user_detail = await httpGetRequest('/user/'+id, true)
   } catch(error){
    console.log(error)
   }
  }
   useEffect(() => {
    let logged_in_user = JSON.parse(localStorage.getItem('_au'))
    if(logged_in_user){
      getUserDetail(logged_in_user.id)
    }

   }, [])
  return (
    <>
     <ToastContainer />
        <AdminPartials.AdminTopMenu/>
        <div id="layoutSidenav">
         <AdminPartials.AdminSidebar/>
            <div id="layoutSidenav_content">
                <main>
                   <Outlet/>
                </main>
              <AdminPartials.AdminFooter/>
            </div>
        </div>
    </>
  )
}

export default AdminLayout