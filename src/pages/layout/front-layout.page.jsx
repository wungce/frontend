import React from 'react'
import { Outlet } from 'react-router-dom'
import FrontComponent from '../../components/common/fronts'

const FrontLayout = () => {
  return (
    <>
    <FrontComponent.NavbarComponent />
    <Outlet />
    <FrontComponent.FooterComponent />
    </>
  )
}

export default FrontLayout