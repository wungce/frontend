import Breadcrumb from "../partials/breadcrumb.partials"
import DataTable from 'react-data-table-component';
import { useState, useEffect } from "react";
import { httpDeleteRequest, httpGetRequest } from "../../service/axios.service";
// import { ImageView} from "../../../components/common/image-view/image-view.component";
import { ActionButtons } from "../../../components/common/action-btns/action-button.component";
import { toast } from "react-toastify";
import { ImageView } from "../../../components/common/image-view/image-view.component";

const PostsListPage = () => {
  const deleteSport = async (id) => {
    try{
        let response = await httpDeleteRequest('/posts/'+id, true)
        if(response.status){
            toast.success(response.msg)
            getAllSports()
        } else{
            toast.error(response.msg)
        }
    } catch(err){
        console.error("DeleteErr:", err)
    }
}
const columns = [
    {
        name: 'Title',
        selector: row => `${row.title.slice(0, 30)}...`,
        sortable: true
    },
    {
        name: 'Image',
        selector: row => <ImageView path={row.image} dir="posts"/>,
    },
    {
      name: 'Author',
        selector: row => row.author,
        sortable: true
    },
    {
      name: 'Category',
        selector: row => row.category.title,
        sortable: true
    },
    {
        name: 'Status',
        selector: row => row.status,
        sortable: true
    },
    {
        name: 'Action',
        selector: row => <ActionButtons id={row._id} deleteAction={deleteSport} path="/admin/posts/"/>,
    }
];

const getAllSports = async () => {
    try{
        let response = await httpGetRequest('/posts')
        console.log(response.result)
        if(response.status ){
            setData(response.result)
        }
    } catch(err){
        console.log("Exception: ", err)
    }
}

const [data, setData] = useState()
useEffect(() => {
    getAllSports()
}, [])
  return (
    <>
    <div className="container-fluid px-4">
        <Breadcrumb
            context="Posts"
            createUrl="/admin/posts/create"
            type="list"
        />
        <div className="card mb-4">
            <div className="card-body">
                <DataTable
                    columns={columns}
                    data={data}
                    pagination
                />
            </div>
        </div>
    </div>
</>
  )
}

export default PostsListPage