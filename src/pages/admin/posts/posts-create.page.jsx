import { useState,useCallback, useEffect } from "react"
import { useFormik } from "formik"
import { Button, Col, Form } from "react-bootstrap"
import Breadcrumb from "../partials/breadcrumb.partials"
import * as Yup from 'yup'
// import { httpPostRequest } from '../../service/axios.service'
import { httpPostRequest, httpGetRequest } from '../../service/axios.service'
import { toast } from "react-toastify"
import { useNavigate } from "react-router-dom"

import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';

import Select from 'react-select';

const PostsCreatePage = () => {
    let default_value = {
        title: '', 
        author: '',
        description: '',
        category: '',
        status: '',
        image: '',
    }
    let validationSchema = Yup.object().shape({
        title: Yup.string().required('Title is required.'),
        author: Yup.string().required("Author is required"),
        description: Yup.string().required("Description is required"),
        category: Yup.array().nullable(),
        status: Yup.string().required("Status is required"),
        image: Yup.object().nullable()
    })
    let [allCats, setAllCats] = useState()

    let getAllCategories = useCallback(
        async () => {
            try {
                let response = await httpGetRequest('/categories', true)
                if (response.status) {
                    let cats = response.result.map((item) => {
                        return {
                            label: item.title,
                            value: item._id
                        }
                    })
                    setAllCats(cats)
                }
            } catch (err) {
                console.error("Error :", err)
            }
        }
        , [])

        useEffect(() => {
            getAllCategories()
        }, [getAllCategories])

    let navigate = useNavigate()


    const formik = useFormik({
        initialValues: default_value,
        validationSchema: validationSchema,
        onSubmit: async ( data) => {

            const values ={...data, category:data.category.value}
            // console.log("data---",data)

            try {
             
                const form_data = new FormData()

                if (values.image) {
                    form_data.append('image', values.image, values.image.name)
                    delete values.image
                }
                Object.keys(values).map((key) => {
                    form_data.append(key, values[key])
                    return null;
                })
                console.log("Values", values)
                let response = await httpPostRequest('/posts', form_data, true, true)
                console.log("response:", response)
                if (response.status) {
                    toast.success(response.msg)
                    navigate('/admin/posts')
                } else {
                    toast.error(response.msg)
                }
            } catch (err) {
                console.error("Error: ", err)
            }
        }
    })


    return (
        <>
            <div className="container-fluid px-4">
                <Breadcrumb
                    context="Post"
                    createUrl=""
                    type="Create"
                />
                <div className="card mb-4">
                    <div className="card-body">
                        <form onSubmit={formik.handleSubmit}>
                            <Form.Group className="row mb-3" controlId="title">
                                <Form.Label className="col-sm-3">Title:</Form.Label>
                                <Col sm={9}>
                                    <Form.Control
                                        type="text"
                                        placeholder="Enter Post Title"
                                        name="title"
                                        size="sm"
                                        required
                                        value={formik.values.title}
                                        onChange={formik.handleChange}
                                    />
                                    {
                                        formik.errors.title && <em className="text-danger">{formik.errors.title}</em>
                                    }
                                </Col>
                            </Form.Group>
                            <Form.Group className="row mb-3" controlId="title">
                                <Form.Label className="col-sm-3">Author:</Form.Label>
                                <Col sm={9}>
                                    <Form.Control
                                        type="text"
                                        placeholder="Enter Author"
                                        name="author"
                                        size="sm"
                                        required
                                        value={formik.values.author}
                                        onChange={formik.handleChange}
                                    />
                                    {
                                        formik.errors.author && <em className="text-danger">{formik.errors.author}</em>
                                    }
                                </Col>
                            </Form.Group>
                            {/* <Form.Group className="row mb-3" controlId="name">
                            <Form.Label className="col-sm-3">Description:</Form.Label>
                           <Col sm={9}>
                           <Form.Control
                                type="text"
                                placeholder="Enter Description"
                                name="description"
                                size="sm"
                                required
                                value={formik.values.description}
                                onChange={formik.handleChange}
                            />
                            {
                                formik.errors.description && <em className="text-danger">{formik.errors.description}</em>
                            } 
                           </Col>
                        </Form.Group> */}

                            <Form.Group className="row mb-3" controlId="description">
                                <Form.Label className="col-sm-3">Description:</Form.Label>
                                <Col sm={9}>
                                    <CKEditor
                                        editor={ClassicEditor}
                                        data={formik.values.description}
                                        onChange={(event, editor) => {
                                            const data = editor.getData();
                                            formik.setValues({
                                                ...formik.values,
                                                description: data
                                            })
                                        }}
                                        name="description"
                                    />
                                    {
                                        formik.errors.description && <em className="text-danger">{formik.errors.description}</em>
                                    }
                                </Col>
                            </Form.Group>

                          
                             {/* Category */}
                        <Form.Group className="row mb-3" controlId="category">
                            <Form.Label name="category" className="col-sm-3">Category:</Form.Label>
                            <Col sm={9}>
                                <Select options={allCats}
                                        
                                    onChange={(e) => {
                                        formik.setValues({
                                            ...formik.values,
                                            category: e
                                        })
                                    }}
                                    value={formik.values.category}
                                    name="category"
                                    required
                                />


                                {
                                    formik.errors.category && <em className="text-danger">{formik.errors.category}</em>
                                }
                            </Col>
                        </Form.Group>


                            <Form.Group className="row mb-3" controlId="status">
                                <Form.Label className="col-sm-3">Status :</Form.Label>
                                <Col sm={9}>
                                    <Form.Select size="sm" value={formik.values.status} onChange={formik.handleChange}>
                                        <option value="">--select Any One</option>
                                        <option value="active">Active</option>
                                        <option value="inactive">In-Active</option>
                                    </Form.Select>
                                    {
                                        formik.errors.status && <em className="text-danger">{formik.errors.status}</em>
                                    }
                                </Col>
                            </Form.Group>

                            <Form.Group className="row mb-3" controlId="image">
                                <Form.Label className="col-sm-3">Image:</Form.Label>
                                <Col sm={3}>
                                    <Form.Control
                                        type="file"
                                        name="image"
                                        size="sm"
                                        onChange={(e) => {
                                            formik.setValues({
                                                ...formik.values,
                                                image: e.target.files[0]
                                            })
                                        }}
                                    />
                                    {
                                        formik.errors.image && <em className="text-danger">{formik.errors.image}</em>
                                    }
                                </Col>
                                <Col sm={3}>
                                    {
                                        <img className="img img-fluid" src={formik.values.image && URL.createObjectURL(formik.values.image)} alt="" />
                                    }
                                </Col>
                            </Form.Group>

                            <Form.Group className="row mb-3" controlId="submit">
                                <Col sm={{ offset: 3, span: 9 }}>
                                    <Button variant="danger" size="sm" type="reset" className="me-2">
                                        <i className="fa fa-trash "></i>Rest
                                    </Button>
                                    <Button variant="success" size="sm" type="submit">
                                        <i className="fa fa-paper-plane "></i>Submit
                                    </Button>
                                </Col>
                            </Form.Group>


                        </form>
                    </div>
                </div>
            </div>
        </>
    )
}

export default PostsCreatePage