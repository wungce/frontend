import PostsCreatePage from "./posts-create.page";
import PostsLayout from "./posts-layout.page";
import PostsListPage from "./posts-list.page";
import PostEditPage from "./posts-edit.page";

const PostsPage = {
    PostsCreatePage,
    PostsLayout,
    PostsListPage,
    PostEditPage
     
}

export default PostsPage