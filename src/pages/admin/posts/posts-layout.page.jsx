import React from 'react'
import { Outlet } from 'react-router-dom'

const PostsLayout = () => {
  return (
    <>
   <Outlet />
    </>
  )
}

export default PostsLayout