import { useFormik } from "formik"
import { Button, Col, Form } from "react-bootstrap"
import Breadcrumb from "../partials/breadcrumb.partials"
import * as Yup from 'yup'
import { httpGetRequest, httpPutRequest } from '../../service/axios.service'
import { toast } from "react-toastify"
import { useNavigate, useParams } from "react-router-dom"
import { useCallback, useEffect, useState } from "react"

import Select from 'react-select';
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
const PostsEditPage = () => {
    let default_value = {
        title: '',
        author: '',
        description: '',
        category: '',
        status: '',
        image: ''
    }

    let validationSchema = Yup.object().shape({
        title: Yup.string().required('Title is required.'),
        author: Yup.string().required('Author is required.'),
        description: Yup.string().required('Description is required.'),
        category: Yup.array().nullable(),
        status: Yup.string().required("Status is required"),
        image: Yup.object().nullable()
    })
    let [allCats, setAllCats] = useState()
    let [loading, setLoading] = useState(true)
    let param = useParams()
    let navigate = useNavigate()

    let getAllCategories = useCallback(
        async () => {
            try {
                let response = await httpGetRequest('/categories', true)
                console.log("All categories are list", response)
                if (response.status) {
                    let cats = response.result.map((item) => {
                        return {
                            label: item.title,
                            value: item._id
                        }
                    })
                    setAllCats(cats)
                }
            } catch (err) {
                console.error("Error :", err)
            }finally{
                setLoading(!loading)
            }
        }
        , [])


    const formik = useFormik({
        initialValues: default_value,
        validationSchema: validationSchema,
        onSubmit: async (values) => {
            try {
                console.log(values)
                const form_data = new FormData()


                // if (values.image && typeof (values.image) === 'object') {
                //     form_data.append('image', values.image, values.image.name)
                //     delete values.image
                // } else {
                //     delete values.image
                // }

                if (values.image && typeof(values.image) === 'object') {
                    form_data.append('image', values.image, values.image.name)
                    delete values.image
                }else{
                    delete values.image
                }

                if (values.category) {
                    values.category = values.category.value
                    // values.category = values.category.map((item) => item.value)
                }

                Object.keys(values).map((key) => {
                    form_data.append(key, values[key])
                    return null;
                })

                let response = await httpPutRequest("/posts/" + param.id, form_data, true, true)
                console.log("response:", response)
                if (response.status) {
                    toast.success(response.msg)
                    navigate('/admin/posts')
                } else {
                    toast.error(response.msg)
                }
            } catch (err) {
                console.error("Error: ", err)
            }
        }
    })

    const getPostsDetail = useCallback(
        async () => {
            let id = param.id
            try {
                let response = await httpGetRequest('/posts/' + id)
                console.log("response", response)
                console.log("response", response.result.category)
                if (response.status) {

                    let sel_cat = response.result.category
                      
                    // let sel_cat = response.result.category.map((item) => {
                    //     return {
                    //         label : item.title,
                    //         value : item._id
                    //     }
                    // })

                    formik.setValues({
                        ...formik.values,
                        ...response.result,
                        category : sel_cat
                    })
                }
            } catch (err) {
                console.error(err)
            }
    
        },
    [param.id, formik])


    useEffect(() => {
        getAllCategories()
        getPostsDetail()
    }, [getAllCategories])

    useEffect(() => {
        getPostsDetail()
    }, [loading])

    return (
        <>
            <div className="container-fluid px-4">
                <Breadcrumb
                    context="Posts"
                    createUrl=""
                    type="Update"
                />
                <div className="card mb-4">
                    <div className="card-body">
                        <form onSubmit={formik.handleSubmit}>
                            <Form.Group className="row mb-3" controlId="title">
                                <Form.Label className="col-sm-3">Title:</Form.Label>
                                <Col sm={9}>
                                    <Form.Control
                                        type="text"
                                        placeholder="Enter Title"
                                        name="title"
                                        size="sm"
                                        required
                                        value={formik.values.title}
                                        onChange={formik.handleChange}
                                    />
                                    {
                                        formik.errors.title && <em className="text-danger">{formik.errors.title}</em>
                                    }
                                </Col>
                            </Form.Group>
                            <Form.Group className="row mb-3" controlId="title">
                                <Form.Label className="col-sm-3">Author:</Form.Label>
                                <Col sm={9}>
                                    <Form.Control
                                        type="text"
                                        placeholder="Author name"
                                        name="author"
                                        size="sm"
                                        required
                                        value={formik.values.author}
                                        onChange={formik.handleChange}
                                    />
                                    {
                                        formik.errors.author && <em className="text-danger">{formik.errors.author}</em>
                                    }
                                </Col>
                            </Form.Group>

                            <Form.Group className="row mb-3" controlId="description">
                                <Form.Label className="col-sm-3">Description:</Form.Label>
                                <Col sm={9}>
                                    <CKEditor
                                        editor={ClassicEditor}
                                        data={formik.values.description}
                                        onChange={(event, editor) => {
                                            const data = editor.getData();
                                            formik.setValues({
                                                ...formik.values,
                                                description: data
                                            })
                                        }}
                                        name="description"
                                    />
                                    {
                                        formik.errors.description && <em className="text-danger">{formik.errors.description}</em>
                                    }
                                </Col>
                            </Form.Group>

                            <Form.Group className="row mb-3" controlId="category">
                                <Form.Label name="category" className="col-sm-3">Category:</Form.Label>
                                <Col sm={9}>
                                    <Select options={allCats}

                                        onChange={(e) => {
                                            formik.setValues({
                                                ...formik.values,
                                                category: e
                                            })
                                        }}
                                        value={formik.values.category}
                                        name="category"
                                        required
                                    />


                                    {
                                        formik.errors.category && <em className="text-danger">{formik.errors.category}</em>
                                    }
                                </Col>
                            </Form.Group>


                            <Form.Group className="row mb-3" controlId="status">
                                <Form.Label className="col-sm-3">Status :</Form.Label>
                                <Col sm={9}>
                                    <Form.Select size="sm" value={formik.values.status} onChange={formik.handleChange}>
                                        <option value="">--select Any One</option>
                                        <option value="active">Active</option>
                                        <option value="inactive">In-Active</option>
                                    </Form.Select>
                                    {
                                        formik.errors.status && <em className="text-danger">{formik.errors.status}</em>
                                    }
                                </Col>
                            </Form.Group>


                            <Form.Group className="row mb-3" controlId="image">
                                <Form.Label className="col-sm-3">Image:</Form.Label>
                                <Col sm={3}>
                                    <Form.Control
                                        type="file"
                                        name="image"
                                        size="sm"
                                        onChange={(e) => {
                                            formik.setValues({
                                                ...formik.values,
                                                image: e.target.files[0]
                                            })
                                        }}
                                    />
                                    {
                                        formik.errors.image && <em className="text-danger">{formik.errors.image}</em>
                                    }
                                </Col>
                                <Col sm={3}>
                                    {
                                        formik.values.image && typeof (formik.values.image) === 'string' ?
                                            <img className="img img-fluid" src={process.env.REACT_APP_IMAGE_URL + "/posts/" + formik.values.image} alt="" />
                                            :
                                            <img className="img img-fluid" src={formik.values.image && URL.createObjectURL(formik.values.image)} alt="" />
                                    }
                                </Col>
                            </Form.Group>

                            <Form.Group className="row mb-3" controlId="submit">
                                <Col sm={{ offset: 3, span: 9 }}>
                                    <Button variant="danger" size="sm" type="reset" className="me-2">
                                        <i className="fa fa-trash "></i>Rest
                                    </Button>
                                    <Button variant="success" size="sm" type="submit">
                                        <i className="fa fa-paper-plane "></i>Submit
                                    </Button>
                                </Col>
                            </Form.Group>
                        </form>
                    </div>
                </div>
            </div>
        </>
    )
}

export default PostsEditPage