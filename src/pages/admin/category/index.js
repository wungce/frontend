import CategoryCreatePage from "./category-create.page";
import CategoryLayout from "./category-layout.page";
import CategoryListPage from "./category-list.page";
import CategoriesEditPage from "./category-edit.page";

const CategoryPage = {
    CategoryCreatePage,
    CategoryLayout,
    CategoryListPage,
    CategoriesEditPage
     
}

export default CategoryPage