import { useFormik } from "formik"
import { Button, Col, Form } from "react-bootstrap"
import Breadcrumb from "../partials/breadcrumb.partials"
import * as Yup from 'yup'
import { httpPostRequest} from "../../service/axios.service";
import { toast } from "react-toastify"
import { useNavigate } from "react-router-dom"


const CategoryCreatePage = () => {
    let default_value = {
        title: ''
    }
    let validationSchema = Yup.object().shape({
        title: Yup.string().required('Title is required.'),
    })

    let navigate = useNavigate()

    const formik = useFormik({
        initialValues: default_value,
        validationSchema: validationSchema,
        onSubmit: async (values) => {
            try {
                console.log("Values", values)
                const form_data = new FormData()
              
                Object.keys(values).map((key) => {
                    form_data.append(key, values[key])
                    return null;
                })
                let response = await httpPostRequest('/categories', form_data, true, true)
                console.log("response:", response)
                if (response.status) {
                    toast.success(response.msg)
                    navigate('/admin/categories')
                } else {
                    toast.error(response.msg)
                }
            } catch (err) {
                console.error("Error: ", err)
            }
        }
    })

    return (
        <>
            <div className="container-fluid px-4">
                <Breadcrumb
                    context="Category"
                    createUrl=""
                    type="Create"
                />
                <div className="card mb-4">
                    <div className="card-body">
                        <form onSubmit={formik.handleSubmit}>

                            {/* title */}
                            <Form.Group className="row mb-3" controlId="title">
                                <Form.Label className="col-sm-3">Title:</Form.Label>
                                <Col sm={9}>
                                    <Form.Control
                                        type="text"
                                        placeholder="Enter sport Title"
                                        name="title"
                                        size="sm"
                                        required
                                        value={formik.values.title}
                                        onChange={formik.handleChange}
                                    />
                                    {
                                        formik.errors.title && <em className="text-danger">{formik.errors.title}</em>
                                    }
                                </Col>
                            </Form.Group>

                            <Form.Group className="row mb-3" controlId="status">
                                <Form.Label className="col-sm-3">Status :</Form.Label>
                                <Col sm={9}>
                                    <Form.Select size="sm" value={formik.values.status} onChange={formik.handleChange}>
                                        <option value="">--select Any One</option>
                                        <option value="active">Active</option>
                                        <option value="inactive">In-Active</option>
                                    </Form.Select>
                                    {
                                        formik.errors.status && <em className="text-danger">{formik.errors.status}</em>
                                    }
                                </Col>
                            </Form.Group>

                            <Form.Group className="row mb-3" controlId="submit">
                                <Col sm={{ offset: 3, span: 9 }}>
                                    <Button variant="danger" size="sm" type="reset" className="me-2">
                                        <i className="fa fa-trash "></i>Rest
                                    </Button>
                                    <Button variant="success" size="sm" type="submit">
                                        <i className="fa fa-paper-plane "></i>Submit
                                    </Button>
                                </Col>
                            </Form.Group>


                        </form>
                    </div>
                </div>
            </div>
        </>
    )
}

export default CategoryCreatePage