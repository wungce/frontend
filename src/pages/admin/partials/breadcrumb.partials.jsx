import React from 'react'
import { NavLink } from 'react-router-dom'

const Breadcrumb = ({context, createUrl, type}) => {
  return (
    <>
    <h1 className="mt-4">
        {context} {type}
        {
            createUrl && <NavLink to={createUrl} className="btn btn-sm float-end btn-success">
                <i className='fa fa-plus' /> Create {context}</NavLink>
        }
    </h1>
    <ol className="breadcrumb mb-4">
        <li className="breadcrumb-item"><NavLink to="/admin">Dashboard</NavLink></li>
        <li className="breadcrumb-item active">{context} {type}</li>
    </ol>
</>
  )
}

export default Breadcrumb