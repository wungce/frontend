import { Link, NavLink, useNavigate} from 'react-router-dom'
import { ucfirst } from '../../../config/helpers';
const AdminTopMenu = () => {
    let navigate = useNavigate()
    let user_info = JSON.parse(localStorage.getItem('_au'))
    const sidebarToggle = (e) => {
        e.preventDefault();
        document.body.classList.toggle('sb-sidenav-toggled');
    }
    const logout = (e) => {
        e.preventDefault()
        localStorage.clear()
        navigate("/login")
    }
    return (
        <>
           <nav className="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <NavLink className="navbar-brand ps-3" to={'/'+user_info.role}>{ucfirst(user_info.role)}</NavLink>
            <button
            onClick={sidebarToggle}
             className="btn btn-link btn-sm order-1 order-lg-0 me-4 me-lg-0" id="sidebarToggle"><i className="fas fa-bars"></i>
             </button>
            <div className="d-none d-md-inline-block form-inline ms-auto me-0 me-md-3 my-2 my-md-0">
               
            </div>
            <ul className="navbar-nav ms-auto ms-md-0 me-3 me-lg-4">
                <li className="nav-item dropdown">
                    <Link className="nav-link dropdown-toggle" id="navbarDropdown" to="/" role="button" data-bs-toggle="dropdown" aria-expanded="false"><i className="fas fa-user fa-fw">
                        </i>
                        {
                            user_info.name
                        }
                        </Link>
                    <ul className="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                        <li><NavLink className="dropdown-item" to="/login" onClick={logout}>Logout</NavLink></li>
                    </ul>
                </li>
            </ul>
        </nav>
        </>
    )
}
export default AdminTopMenu