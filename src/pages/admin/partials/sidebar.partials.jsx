import React from 'react'
import { NavLink } from 'react-router-dom'
const AdminSidebar = () => {
    return (
        <>
            <div id="layoutSidenav_nav">
                <nav className="sb-sidenav accordion sb-sidenav-light" id="sidenavAccordion">
                    <div className="sb-sidenav-menu">
                        <div className="nav">
                            <div className="sb-sidenav-menu-heading">Core</div>
                            <NavLink className="nav-link" to="/admin">
                                <div className="sb-nav-link-icon"><i className="fas fa-tachometer-alt"></i></div>
                                Dashboard
                            </NavLink>
                            <NavLink className="nav-link" to="/">
                                <div className="sb-nav-link-icon"><i className="fas fa-tachometer-alt"></i></div>
                                Go Home
                            </NavLink>
                            <NavLink className="nav-link" to="/admin/categories">
                                <div className="sb-nav-link-icon"><i className="fas fa-tachometer-alt"></i></div>
                                Categories
                            </NavLink>
                            <NavLink className="nav-link" to="/admin/posts">
                                <div className="sb-nav-link-icon"><i className="fas fa-tachometer-alt"></i></div>
                                Post
                            </NavLink>
                            <NavLink className="nav-link" to="/admin/user">
                                <div className="sb-nav-link-icon"><i className="fas fa-tachometer-alt"></i></div>
                                Users
                            </NavLink>

                        </div>
                    </div>

                </nav>
            </div>
        </>
    )
}

export default AdminSidebar