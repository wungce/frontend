import AdminFooter from "./footer.partials"
import AdminSidebar from "./sidebar.partials"
import AdminTopMenu from "./to-men.partials"
const AdminPartials = {
    AdminTopMenu,
    AdminSidebar,
    AdminFooter
}

export default AdminPartials