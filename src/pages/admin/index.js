import DashboardPage from './dashaboard.page'
import User from './user'
import Category from './category'
import Post from './posts'
const AdminPage = {
    DashboardPage,
    User,
    Category,
    Post
}

export default AdminPage