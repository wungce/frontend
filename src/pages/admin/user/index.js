import UserLayout from './user-layout.page'
import UserList from './user-list.page' 

const User = {
    UserLayout,
    UserList
}
export default User