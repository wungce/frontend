import Breadcrumb from '../partials/breadcrumb.partials'
import DataTable from 'react-data-table-component';
import { useState, useEffect } from "react";
import {  httpGetRequest } from "../../service/axios.service";
const UserList = () => {
    const columns = [
        {
            name: 'Eamil',
            selector: row => row.email,
            sortable: true
        },
        {
            name: 'UserName',
            selector: row => row.name,
            sortable: true
        },
        {
            name: 'Role',
            selector: row => row.role,
            sortable: true
        },
       
    ];

    const getAllOrders = async () => {
        try {
            let response = await httpGetRequest('/user', true)
            console.log(response.result)
            if (response.status) {
                setData(response.result)
            }
        } catch (err) {
            console.log("Exception: ", err)
        }
    }

    const [data, setData] = useState()
    useEffect(() => {
        getAllOrders()
    }, [])

  return (
    <>
    <div className="container-fluid px-4">
        <Breadcrumb
            context="User"
            type="list"
        />
        <div className="card mb-4">
            <div className="card-body">
                <DataTable
                    columns={columns}
                    data={data}
                    pagination
                />
            </div>
        </div>
    </div>
</>
  )
}

export default UserList