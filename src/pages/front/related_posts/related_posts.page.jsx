import React, { useState, useEffect, useCallback } from 'react'
import { httpGetRequest } from '../../service/axios.service'
import { NavLink } from 'react-router-dom'
import { useParams } from 'react-router-dom'
const RelatedPosts = () => {
    const [relatedPosts, setRelatedPosts] = useState()
    let {slug} = useParams()


    const getNewsDetail = useCallback(async () => {
        // let slug = params.slug
        let posts_detail = await httpGetRequest('/posts/detail/' + slug)
        if (posts_detail.status) {
            setRelatedPosts(posts_detail.result.related_posts)
        }
    }, [slug])

    useEffect(() => {
        getNewsDetail()
    }, [slug])
    return (
        <>
            <div class="col-lg-4">
                <div class="blog_right_sidebar">

                    <aside class="single_sidebar_widget popular_post_widget">
                        <h3 class="widget_title">Related Post</h3>
                        {
                            relatedPosts && relatedPosts.map((item, index) => {
                                return  <div class="media post_item" key={index}>
                                <NavLink   to={"/posts/detail/" + item.slug} className="link">
                                    <img src={process.env.REACT_APP_IMAGE_URL + "/posts/" + item.image} alt="post" width={100} />
                                </NavLink>
                                <div class="media-body">
                                    <NavLink to={"/posts/detail/" + item.slug}>
                                        <h3>{item.title}</h3>
                                    </NavLink>
                                </div>
                            </div>
                            })
                        }
                    </aside>
                </div>
            </div>
        </>
    )
}

export default RelatedPosts