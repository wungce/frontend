import ErrorPage from "./error-front.page"
import HomePage from "./home-front.page"
import LoginPage from "./login-front.page"
import RegisterPage from "./register-front.page"
import DetailPage from './detail-front.page'
import SportPage from './sports/sport-front.page'
import HealthPage from "./healths/health-front.page"
import LifeStyle from "./lifestyle/lifestyle-front.page"
import Travel from "./travel/travel-front.page"
const FrontPage = {
    LoginPage,
    RegisterPage,
    
    HomePage,
    DetailPage,

    HealthPage,
    SportPage,
    LifeStyle,
    Travel,
    

    ErrorPage
    
}

export default FrontPage
