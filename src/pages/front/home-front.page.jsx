import { useState, useEffect } from 'react'
import { Container } from 'react-bootstrap'

import '../../assets/css/short.css'

import ShortDetailPage from './shortDetail-front.page'

import TrendingLeftSide from '../../components/common/fronts/feeds/trending-left.page'
import ContentRightSide from '../../components/common/fronts/feeds/content-right.page'
import WeeklyTopNews from '../../components/common/fronts/feeds/weekly-topnews.page'
import { httpGetRequest } from '../service/axios.service'

const HomePage = () => {
    const [data, setData] = useState()
    //  const [relatedPosts, setRelatedPosts] = useState()
    const getAllNews = async () => {
        try {
            let response = await httpGetRequest('/posts')
            console.log(response)
            if (response.status) {
                let active_news = response.result.filter((item) => (item.status === "active"))
                setData(active_news)
                //   setRelatedPosts(response.result.related_posts)
            }
        } catch (error) {
            console.log("Error", error)
        }
    }
    useEffect(() => {
        getAllNews()
    }, [])
    return (
        <>
            <main>
                {/* <!-- Trending Area Start --> */}
                <div className="trending-area fix mt-4">
                    <div className="container">
                        <div className="trending-main">
                            {/* <!-- Trending Tittle --> */}

                            <div className="row">
                                <TrendingLeftSide/>
                                {/* <!-- Riht content --> */}
                                <ContentRightSide />
                            </div>
                        </div>
                    </div>
                </div>
                {/* <!--   Weekly-News start --> */}
                <div className="weekly-news-area pt-50">
                    <div className="container">
                        <div className="weekly-wrapper">
                            {/* <!-- section Tittle --> */}
                            <WeeklyTopNews />
                        </div>
                    </div>
                </div>

                <Container className='mt-4'>
                    <h3>National news</h3>
                    <div className="row mt-4">
                        {
                            data && data.map((item, index) => (
                                <div className="col-xs-12 col-sm-12 col-md-3" key={index}>
                                    <ShortDetailPage  item={item} type="posts"/>
                                </div>
                            ))
                        }
                    </div>
                </Container>
            </main>
        </>
    )
}

export default HomePage