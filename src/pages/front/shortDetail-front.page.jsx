import React from 'react'
import { NavLink } from 'react-router-dom'

const ShortDetailPage = ({ item, type }) => {
    return (
        <>
            <NavLink to={"/"+type+"/detail/" + item.slug}>
                <img className='image' src={process.env.REACT_APP_IMAGE_URL + "/" + type + "/" + item.image} alt="" width={242} height={130} />
                <div className='col-xs-12  mt-3'>
                    <h5>
                        {
                            `${item.title.substring(0, 22)}...`
                        }
                    </h5>
                    <p className="size">
                        <small className='description'dangerouslySetInnerHTML={{ __html: `${item.description.substring(0, 100)}...` }}>
                           
                        </small>
                    </p>
                </div>
            </NavLink>
        </>
    )
}

export default ShortDetailPage