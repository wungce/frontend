import React, { useEffect, useState } from 'react'
import { httpGetRequest } from '../../service/axios.service'
import ShortDetailPage from '../shortDetail-front.page'
import '../../../assets/css/short.css'

const HealthPage = () => {
    const [data, setData] = useState()
    const getAllHealths = async () => {
        try {
            let response = await httpGetRequest('/posts')
            if (response.status) {
                let category_healths = response.result.filter((item) => (item.status === 'active' && item.category.title === "health"))
                setData(category_healths)
            }
        } catch (error) {
            console.log(error)
        }
    }
    useEffect(() => {
        getAllHealths()
    }, [])
    return (
        <>
            <div className="container">
                <div className="row mt-4 ms-2">
                    {
                        data && data.map((item, index) => (
                            
                            <div className="col-xs-12 col-md-3" key={index}>
                                <ShortDetailPage item={item} type="posts" />
                            </div>
                        ))
                    }
                </div>
            </div>

        </>
    )
}

export default HealthPage