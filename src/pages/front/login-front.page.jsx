import { useEffect, useState } from "react";

import { Container, Row, Col, Form, Button } from "react-bootstrap";

import { NavLink, useNavigate } from 'react-router-dom'

import { useFormik } from "formik";
import * as Yup from "yup"

import { httpPostRequest } from "../service/axios.service";

import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

// import GifLoading from '../../assets/img/loading1.gif'

const LoginPage = (props) => {
  let navigate = useNavigate()

  let default_data = {
    email: '',
    password: '',
    remember_me: false,
  };
  let [loading, setLoading] = useState(false)

  const loginValidationSchema = Yup.object().shape({
    email: Yup.string().email('Email invalidate format.').required('Email is required.'),
    password: Yup.string().required('Password is required.')
  })

  const formik = useFormik({
    initialValues: default_data,
    validationSchema: loginValidationSchema,
    onSubmit: async (values) => {
      try {
        setLoading(true)
        let response = await httpPostRequest('/login', values)
        console.log("response", response)
        if (response.status) {
          let user_info = {
            id: response.result.user._id,
            name: response.result.user.name,
            image: response.result.user.image,
            role: response.result.user.role
          }
          localStorage.setItem('access_token', response.result.access_token)
          localStorage.setItem('_au', JSON.stringify(user_info))
          toast.success(response.msg)
          navigate('/' + user_info.role)
        }
      } catch (err) {
        console.log(err.response.data.msg)
        toast.error(err.response.data.msg)
      }
      finally{
        setLoading(false)
      }
    }
  })

  useEffect(() => {
    let token = localStorage.getItem('access_token')
    if (token) {
      let user = JSON.parse(localStorage.getItem('_au'))
      navigate('/'+user.role)
    }
  },[navigate])

  return (
    <>
      {loading ? (
        <Container>
          loading .....
        </Container>
      ) : (
        <Container >
          <ToastContainer/>
          <Row className="mt-5">
            <Col sm={{ offset: 3, span: 7 }}>
              <h1>Login form</h1>
              <hr />
              <Form onSubmit={formik.handleSubmit}>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                  <Form.Label>Email address</Form.Label>
                  <Form.Control
                    type="email"
                    placeholder="Enter email"
                    name="email"
                    size="sm"
                    required
                    value={formik.values.email}
                    onChange={formik.handleChange}
                  />
                  {
                    formik.errors.email && <em className="text-danger">{formik.errors.email}</em>
                  }
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicPassword">
                  <Form.Label>Password</Form.Label>
                  <Form.Control
                    type="password"
                    placeholder="Password"
                    name="password"
                    size="sm"
                    required
                    onChange={formik.handleChange}
                  />
                  {
                    formik.errors.password && <em className="text-danger">{formik.errors.password}</em>
                  }
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicCheckbox">
                  <Form.Check
                    type="checkbox"
                    label="Remember me"
                    name="remember_me"
                    // onChange={(e) => {
                    //   if(e.target.checked){
                    //     formik.setValues({
                    //       ...formik.values,
                    //       remember_me : true
                    //     })
                    //   }else{
                    //     formik.setValues({
                    //       ...formik.values,
                    //       remember_me : false
                    //     })
                    //   }
                    // }}
                    onChange={formik.handleChange}
                  />
                </Form.Group>
                <div className="d-grid gap-2">
                  <Button variant="btn btn-outline-success" type="submit">
                    Submit
                  </Button>OR <NavLink to="/register"> <h6>Create A new Account</h6></NavLink>
                </div>
              </Form>
            </Col>
          </Row>
        </Container>
      )}
    </>
  );
};

export default LoginPage;
