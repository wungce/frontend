import React, { useEffect, useState } from 'react'
import { httpGetRequest } from '../../service/axios.service'
import ShortDetailPage from '../shortDetail-front.page'
import '../../../assets/css/short.css'

const TravelPage = () => {
    const [data, setData] = useState()
    const getAllTravels = async () => {
        try {
            let response = await httpGetRequest('/posts')
            if (response.status) {
                let category_travels = response.result.filter((item) => (item.status === 'active' && item.category.title === "travel"))
                setData(category_travels)
            }
        } catch (error) {
            console.log(error)
        }
    }
    useEffect(() => {
        getAllTravels()
    }, [])
    return (
        <>
            <div className="container">
                <div className="row mt-4 ms-2">
                    {
                        data && data.map((item, index) => (
                            
                            <div className="col-xs-12 col-md-3" key={index}>
                                <ShortDetailPage item={item} type="posts" />
                            </div>
                        ))
                    }
                </div>
            </div>

        </>
    )
}

export default TravelPage