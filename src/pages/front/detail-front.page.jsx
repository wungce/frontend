import { useCallback, useEffect, useState } from 'react'
import { NavLink } from 'react-router-dom'
import { Row, Col, Container } from 'react-bootstrap'
import { useParams } from 'react-router-dom'

import { httpGetRequest } from '../service/axios.service'
import RelatedPosts from './related_posts/related_posts.page'
// import ShortDetailPage from './shortDetail-front.page'
const DetailPage = () => {
   let [data, setData] = useState()

   // const [relatedPosts, setRelatedPosts] = useState()
   let {slug} = useParams()
   const getNewsDetail = useCallback(async () => {
      // let slug = params.slug
      let posts_detail = await httpGetRequest('/posts/detail/' + slug)
      if (posts_detail.status) {
         setData(posts_detail.result.response)
         // setRelatedPosts(posts_detail.result.related_posts)
      }
   }, [slug])


   useEffect(() => {
      getNewsDetail()
   }, [slug])

   return (
      <>
         {
            data && data !== null ?
               <>
                  <section class="blog_area single-post-area section-padding">
                     <div class="container">
                        <div class="row">
                           <div class="col-lg-8 posts-list">
                              <div class="single-post">
                                 <h2>
                                    {
                                       data.title
                                    }
                                 </h2>
                                 <div>
                                    {

                                       `Published : ${data.updatedAt.slice(0, 10)}`
                                    }
                                 </div>
                                 <div>
                                    {
                                       `by ${data.author}`
                                    }
                                 </div>

                                 <div class="feature-img mt-2">
                                    <img class="img-fluid" src={process.env.REACT_APP_IMAGE_URL + "/posts/" + data.image} alt="" width={700} />
                                 </div>
                                 <div class="blog_details">

                                    <p class="excert" dangerouslySetInnerHTML={{ __html: data.description }}>

                                    </p>
                                 </div>
                              </div>



                           </div>
                           {/* <ShortDetailPage /> */}
                           <RelatedPosts />
                           {/* <div class="col-lg-4">
                              <div class="blog_right_sidebar">
                                 
                                 <aside class="single_sidebar_widget popular_post_widget">
                                    <h3 class="widget_title">Related Post</h3>
                                    {
                                       relatedPosts && relatedPosts.map((item, index) => (

                                          <div class="media post_item" key={index}>
                                             <NavLink to={"posts/detail/" + item.slug} className="link">
                                                <img src={process.env.REACT_APP_IMAGE_URL + "/posts/" + item.image} alt="post" width={100} />
                                             </NavLink>
                                             <div class="media-body">
                                                <NavLink to={"posts/detail/" + item.slug}>
                                                   <h3>{item.title}</h3>
                                                </NavLink>
                                             </div>
                                          </div>
                                       ))
                                    }
                                 </aside>
                              </div>
                           </div> */}
                        </div>
                        {/* <div className="row">
                           <div className="col-lg-8">
                              <form className="form-contact contact_form mb-80" action="contact_process.php" method="post" id="contactForm" novalidate="novalidate">
                                 <div className="row">
                                    <div className="col-12">
                                       <div className="form-group">
                                          <textarea className="form-control w-100 error" name="message" id="message" cols="30" rows="9" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Message'" placeholder="Enter Message"></textarea>
                                       </div>
                                    </div>
                                    <div className="col-sm-6">
                                       <div className="form-group">
                                          <input className="form-control error" name="name" id="name" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your name'" placeholder="Enter your name" />
                                       </div>
                                    </div>
                                    <div className="col-sm-6">
                                       <div className="form-group">
                                          <input className="form-control error" name="email" id="email" type="email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter email address'" placeholder="Email" />
                                       </div>
                                    </div>

                                 </div>
                                 <div className="form-group mt-3">
                                    <button type="submit" className="button btn-success button-contactForm boxed-btn  w-25 p-2">Send</button>
                                 </div>
                              </form>
                           </div>
                        </div>
                        <div className="row">
                           <div className="col-xs-12 col-md-8 mt-2">
                              <h5 class="p-1">
                                 username
                              </h5>
                              <p class="p-1 ">
                                 Lorem ipsum dolor sit, amet consectetur adipisicing elit. Aliquam, autem vel, cumque laborum unde mollitia soluta dolor error perspiciatis iure dignissimos labore quam accusantium doloribus ex in consectetur molestiae? Impedit.
                              </p>
                           </div>
                           <div className="col-xs-12 col-md-8 mt-2">
                              <h5 class="p-1">
                                 username
                              </h5>
                              <p class="p-1 ">
                                 Lorem ipsum dolor sit, amet consectetur adipisicing elit. Aliquam, autem vel, cumque laborum unde mollitia soluta dolor error perspiciatis iure dignissimos labore quam accusantium doloribus ex in consectetur molestiae? Impedit.
                              </p>
                           </div>
                        </div> */}
                     </div>
                  </section>
               </>
               :
               <>
                  <Container>
                     <Row>
                        <Col>
                           <h1>.....</h1>
                        </Col>
                     </Row>
                  </Container>
               </>
         }
      </>
   )
}

export default DetailPage