import LightGallery from 'lightgallery/react';

// import styles
import 'lightgallery/css/lightgallery.css';
import 'lightgallery/css/lg-zoom.css';
import 'lightgallery/css/lg-thumbnail.css';

import { NavLink } from 'react-bootstrap';
// Plugins
import lgThumbnail from 'lightgallery/plugins/thumbnail';
import lgZoom from 'lightgallery/plugins/zoom';

export const ImageView = ({ path, dir }) => {
    const onInit =(e) => {}
    return (
        <>
            <LightGallery
                onInit={onInit}
                speed={500}
                plugins={[lgThumbnail, lgZoom]}
            >
                <NavLink href={process.env.REACT_APP_IMAGE_URL+'/'+dir+'/'+path} data-lg-size="1600-2400">
                    view-image
                </NavLink>
            </LightGallery>
        </>
    )
}