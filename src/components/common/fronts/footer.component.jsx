import { Row, Col } from 'react-bootstrap'
import Footer1 from '../../../assets/img/header-icon/logo.png'
import Instra from '../../../assets/img/logo/icon-ins.png'
import Facebook from '../../../assets/img/logo/icon-fb.png'
import Twitter from '../../../assets/img/logo/icon-tw.png'


export const FooterComponent = () => {

    return (
        <>
            <footer className='mt-4'>
                {/* <!-- Footer Start--> */}
                <div className="footer-area footer-padding fix">
                    <div className="container">
                        <div className="row d-flex justify-content-between">
                            <div className="col-xl-5 col-lg-5 col-md-7 col-sm-12">
                                <div className="single-footer-caption">
                                    <div className="single-footer-caption">
                                        {/* <!-- logo --> */}

                                        <div className="footer-logo">
                                            <a href="index.html"><img src={Footer1} alt="" /></a>
                                        </div>
                                        <hr />

                                        <div className="footer-tittle">
                                            <div className="footer-pera">
                                                <p>
                                                    The Rising Nepal is Nepal’s first English broadsheet daily published by the Gorkhapatra Corporation, an undertaking of the Government of Nepal. The risingnepaldaily.com is Online edition of the daily newspaper.
                                                </p>
                                            </div>
                                        </div>
                                        
                                        {/* <!-- social --> */}
                                        <div className="footer-social">
                                            <a href="/"><i className="fab fa-twitter"></i></a>
                                            <a href="/"><i className="fab fa-instagram"></i></a>
                                            <a href="/"><i className="fab fa-pinterest-p"></i></a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div className="col-xl-3 col-lg-3 col-md-4  col-sm-6">
                                <div className="single-footer-caption mt-60">
                                    <div className="footer-tittle">
                                        <Col>
                                        <h4>Contact Us</h4>

                                        </Col>
                                        <hr />

                                        <Col>
                                            <p>About the Post</p>
                                        </Col>
                                        <Col>
                                            <p>Masthead</p>
                                        </Col>
                                        <Col>
                                            <p>Privacy Policy</p>
                                        </Col>
                                        <Col>
                                            <p>Contact Me</p>
                                        </Col>
                                       
                                    </div>
                                </div>
                            </div>
                    
                            <div className="col-xl-3 col-lg-3 col-md-5 col-sm-6">
                                <div className="single-footer-caption mb-50 mt-60">
                                    <div className="footer-tittle">
                                        <h4>Follow on Social Media</h4>
                                        <Row>
                                            <hr />
                                                    <Col>
                                                   <img src={Facebook} alt="" />
                                                    </Col>
                                                    <Col>
                                                    <img src={Instra} alt="" />
                                                    </Col>
                                                    <Col>
                                                    <img src={Twitter} alt="" />
                                                    </Col>
                                        </Row>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* <!-- footer-bottom aera --> */}
                <div className="footer-bottom-area">
                    <div className="container">
                        <div className="footer-border">
                            <div className="row d-flex align-items-center justify-content-between">
                                <div className="col-lg-6">
                                    <div className="footer-copy-right">
                                    </div>
                                </div>
                                <div className="col-lg-6">
                                    <div className="footer-menu f-right">
                                        <ul>
                                            <li><a href="/">Terms of use</a></li>
                                            <li><a href="/">Privacy Policy</a></li>
                                            <li><a href="/">Contact</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* <!-- Footer End--> */}
            </footer>
        </>
    )
}