import 'bootstrap/dist/css/bootstrap.min.css';
import '../../../assets/css/header.css'
import '../../../assets/css/style.css'
import Image2 from '../../../assets/img/header-icon/header_icon1.png'
import Image3 from '../../../assets/img/header-icon/header_icon2.png'
import Logo from '../../../assets/img/header-icon/logo.png'
import Header from '../../../assets/img/header-icon/header_card.jpg'

import { NavLink, useNavigate } from 'react-router-dom';


export const NavbarComponent = () => {
    let user_info = JSON.parse(localStorage.getItem("_au")) ?? null
    let navigate = useNavigate()
    const date = new Date()
    const day = date.getDate()
    const week = date.getDay()
    const years = date.getFullYear()
    const month = date.getMonth()

    const hours = date.getHours()
    const minutes = date.getMinutes()
    console.log(minutes)

    const weekDays = [
        'Sunday',
        'Monday',
        'Tuesday',
        'Wednesday',
        'Thursday',
        'Friday',
        'Saturday'
    ]
    const months = [
        'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'
    ]

    return (<>
        <div className="header-area">
            <div className="main-header ">
                <div className="header-top black-bg d-none d-md-block">
                    <div className="container">
                        <div className="col-xl-12">
                            <div className="row d-flex justify-content-between align-items-center">
                                <div className="header-info-left">
                                    <ul>
                                        <li><img src={Image2} alt="/" />34ºc, Sunny </li>
                                        <li><img src={Image3} alt="/" />{weekDays[week]}, {day} {months[month]}, {years}</li>
                                        <li><img src={Image3} alt="/" />{hours} : {minutes}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="header-mid d-none d-md-block">
                    <div className="container">
                        <div className="row d-flex align-items-center">
                            {/* <!-- Logo --> */}
                            <div className="col-xl-3 col-lg-3 col-md-3">
                                <div className="logo">
                                    <NavLink to="index.html"><img src={Logo} alt="/" /></NavLink>
                                </div>
                            </div>
                            <div className="col-xl-9 col-lg-9 col-md-9">
                                <div className="header-banner f-right ">
                                    <img src={Header} alt="/" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="header-bottom header-sticky ">
                    <div className="container">
                        <div className="row align-items-center">
                            <div className="col-xl-7 col-lg-7 col-md-10 header-flex">
                                {/* <!-- sticky --> */}
                                <div className="sticky-logo info-open">
                                    <NavLink to="index.html"><img src={Logo} alt="/" /></NavLink>
                                </div>
                                {/* <!-- Main-menu --> */}
                                <div className="main-menu d-none d-md-block">
                                    <nav >
                                        <ul id="navigation">
                                            <li><NavLink to="/">Home</NavLink></li>
                                            <li><NavLink to="/healths">Health</NavLink></li>
                                            <li><NavLink to="/sports">Sport</NavLink></li>
                                            <li><NavLink to="/lifestyles">LifeStyle</NavLink></li>
                                            <li><NavLink to="/travels">Travel</NavLink></li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                            <div className="col-xl-5 col-lg-5 col-md-2">
                                {/* <div className="header-right-btn f-right d-none d-lg-block">
                                   
                                    <input type="text" class="form-control" placeholder='Search Keyword'
                                        onfocus="this.placeholder = ''" onblur="this.placeholder = 'Search Keyword'" />
                                
                                </div> */}
                                <div className='col-xl-2 col-lg-2 col-md-2 offset-3'>
                                    <div className="col ">
                                        <div className="main-menu d-none d-md-block">
                                            <nav>
                                                <ul id="navigation">
                                                    <li><NavLink to="">more</NavLink>
                                                        <ul className="submenu">
                                                            {
                                                                !user_info ?
                                                                    <>
                                                                        <li><NavLink to="/register">SignUp</NavLink></li>
                                                                        <li><NavLink to="/login">Login</NavLink></li>
                                                                    </>
                                                                    : null
                                                            }

                                                            {
                                                                user_info ?
                                                                    <>
                                                                        <li><NavLink to={user_info.role}>{user_info.name}</NavLink></li>
                                                                        <li><NavLink to="/login" onClick={(e) => {
                                                                            e.preventDefault()
                                                                            localStorage.clear()
                                                                            navigate('/admin')
                                                                        }}>Logout</NavLink></li>
                                                                    </> : null
                                                            }

                                                        </ul>

                                                    </li>
                                                </ul>
                                            </nav>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {/* <!-- Mobile Menu --> */}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </>)
}
