import React, { useState, useEffect } from 'react'
import { NavLink } from 'react-router-dom'
import Trending1 from '../../../../assets/img/home/trending_top.jpg'
import { httpGetRequest } from '../../../../pages/service/axios.service'

const TrendingLeftSide = () => {
  const [data, setDate] = useState()
  const getAllNews = async () => {
    try {
      let response = await httpGetRequest('/posts')
      if (response.status) {
        let active_news = response.result.filter((item) => (item.status === "active" && item.category.title === "top news"))
        setDate(active_news)
      }
    } catch (error) {
      console.log("Error", error)
    }
  }
  useEffect(() => {
    getAllNews()
  }, [])
  return (
    <>
      <div className="col-lg-8">
        {/* <!-- Trending Top --> */}
        <div className="trending-top mb-30">
          <div className="trend-top-img">
            <img src={Trending1} alt="" />
            <div className="trend-top-cap">
              <span>Appetizers</span>
              <h2><NavLink to="detail">Welcome To The Best Model Winner<br /> Contest At Look of the year</NavLink></h2>
            </div>
          </div>
        </div>
        {/* <!-- Trending Bottom --> */}
        <div className="trending-bottom">
          <h3>Top News</h3>
          <div className="row">
            {
              data && data.map((item, index) => (
                <div className="col-lg-4" key={index}>

                  <div className="single-bottom mb-35">
                    <div className="trend-bottom-img mb-30">
                      <img src={process.env.REACT_APP_IMAGE_URL + "/posts/" + item.image} width="300" height="200" alt="" />
                    </div>
                    <div className="trend-bottom-cap">
                      {/* <span className="color1">{item.category}</span> */}
                      <h4><NavLink to={"/posts/detail/" + item.slug}>{`${item.title.slice(0, 30)}..`}</NavLink></h4>
                    </div>
                  </div>
                </div>
              ))
            }
          </div>
        </div>
      </div>
    </>

  )
}

export default TrendingLeftSide