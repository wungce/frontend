import React from 'react'
import { NavLink } from 'react-router-dom'
import News1 from '../../../../assets/img/news/weeklyNews1.jpg'
import News2 from '../../../../assets/img/news/weeklyNews2.jpg'
import News3 from '../../../../assets/img/news/weeklyNews3.jpg'
import News4 from '../../../../assets/img/news/weeklyNews1.jpg'
const WeeklyTopNews = () => {
    return (
        <>
            <div className="row">
                <div className="col-lg-12">
                    <div className="section-tittle mb-30">
                        <h3>Weekly Top News</h3>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-12">
                    <div className="weekly-news-active dot-style d-flex dot-style">
                        <div className="weekly-single">
                            <div className="weekly-img">
                                <img src={News1} alt="" />
                            </div>
                            <div className="weekly-caption">
                                <span className="color1">Strike</span>
                                <h4><NavLink to="#">Welcome To The Best Model  Winner Contest</NavLink></h4>
                            </div>
                        </div>
                        <div className="weekly-single">
                            <div className="weekly-img">
                                <img src={News2} alt="" />
                            </div>
                            <div className="weekly-caption">
                                <span className="color1">Strike</span>
                                <h4><NavLink to="#">Welcome To The Best Model  Winner Contest</NavLink></h4>
                            </div>
                        </div>
                        <div className="weekly-single">
                            <div className="weekly-img">
                                <img src={News3} alt="" />
                            </div>
                            <div className="weekly-caption">
                                <span className="color1">Strike</span>
                                <h4><NavLink to="#">Welcome To The Best Model  Winner Contest</NavLink></h4>
                            </div>
                        </div>
                        <div className="weekly-single">
                            <div className="weekly-img">
                                <img src={News4} alt="" />
                            </div>
                            <div className="weekly-caption">
                                <span className="color1">Strike</span>
                                <h4><NavLink to="#">Welcome To The Best Model  Winner Contest</NavLink></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default WeeklyTopNews