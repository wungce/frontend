import React, { useEffect, useState } from 'react'
import { NavLink } from 'react-router-dom'
import { httpGetRequest } from '../../../../pages/service/axios.service'
const ContentRightSide = () => {
    const [data, setDate] = useState()
    const getAllNews = async () => {
        try {
            let response = await httpGetRequest('/posts')
            if (response.status) {
                let active_news = response.result.filter((item) => (item.status === "active" && item.category.title === "sport"))
                setDate(active_news)
            }
        } catch (error) {
            console.log("Error", error)
        }
    }
    useEffect(() => {
        getAllNews()
    }, [])
    return (
        <>
            <div className="col-lg-4">
                {
                    data && data.map((item, index) => (
                        <div className="trand-right-single d-flex" key={index}>

                            <div className="trand-right-img">
                                <img src={process.env.REACT_APP_IMAGE_URL + "/posts/" + item.image} width="150" height="100" alt="" />
                            </div>
                            <div className="trand-right-cap">

                                <NavLink to={"/posts/detail/" + item.slug}>
                                    <h4>{`${item.title.slice(0, 60)}..`}</h4>
                                </NavLink>
                            </div>

                        </div>
                    ))
                }
            </div>

        </>
    )
}

export default ContentRightSide